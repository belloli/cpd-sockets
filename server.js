var express = require('express');
var app = express();

app.configure(function() {
    app.use(express.static(__dirname, 'public'));
});

var http = require('http')
var server = http.createServer(app);
var io = require('socket.io').listen(server);
var ss = require('socket.io-stream');
var fs = require('fs');
var path = require('path');

var listaUsuarios = {};

server.listen(3000);

app.get('/', function(req, res) {
    res.sendfile(__dirname + '/index.html');
});

// Conexão: inicia a instância do usuário
io.sockets.on('connection', function(socket) {

    function atualizaUsuarios() {
        io.sockets.emit('atualizarListaUsuarios', {
            usuarios: Object.keys(listaUsuarios)
        });
    }

    socket.on('novoUsuario', function(data, callback) {
        if (listaUsuarios[data.usuario]) {
            callback(false);
        } else {
            callback(true);
            socket.usuario = data.usuario;
            listaUsuarios[socket.usuario] = socket;
            atualizaUsuarios();
        }
    });

    socket.on('enviarMensagem', function(data) {
        listaUsuarios[data.mensagemPara].emit('novaMensagem', {
            mensagemDe: socket.usuario,
            mensagem: data.mensagem
        });
    });

    ss(socket).on('upload', function(stream, data) {
        var filename = path.basename(data.name);
        var publicSrc = 'public/uploads/' + filename;
        stream.pipe(fs.createWriteStream(__dirname + '/' + publicSrc));

        socket.emit('arquivo', publicSrc);
    });

    // Desconectar: remove usuários não conectados
    socket.on('disconnect', function() {
        if (!socket.usuario) {
            return;
        }
        delete listaUsuarios[socket.usuario];
        atualizaUsuarios();
    });
});
