$(function() {
    var socket = io.connect();
    var usuarioAtivo = null;
    var $body = $('body');

    // Gerar nome de usuário como convidado
    function simpleHash() {
        var hash = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.split('');

        hash = hash.sort(function() {
            return 0.5 - Math.random()
        }).join('');
        
        return hash.slice(0, 5);
    }

    $('#usuario').val('convidado-' + simpleHash());

    // Minimizar/Maximizar
    $(document).on('click', 'button.resize', function(event) {
        event.preventDefault();
        var $thisWindow = $('#chat, .window');
        if ($thisWindow.hasClass('minimized')) {
            $thisWindow.removeClass('minimized');
            $(this).text('_');
        } else {
            $thisWindow.addClass('minimized');
            $(this).text('+');
        }
    });

    // Fechar Janela
    $(document).on('click', 'button.close', function(event) {
        event.preventDefault();
        $(this).parent().parent().remove();
    });

    // Submit do Formulário de Login
    $('#loginForm').on('submit', function(event) {
        event.preventDefault();
        var $usuario = $('#usuario');
        var usuario = $usuario.val();
        var $loginError = $('span.error');

        socket.emit('novoUsuario', {
            usuario: usuario
        }, function(validarLogin) {
            if (validarLogin) {
                usuarioAtivo = usuario;
                $(document).attr('title', 'Usuário: ' + usuarioAtivo);
            } else {
                $loginError.text('Erro no login! Usuário já existente.');
            }
        });
    });

    $(document).on('click', 'option', function(event) {
        event.preventDefault();

        var id = $(this).val();
        var $id = $('#' + id);

        if ($id.length === 0) {
            novaJanela(id);
        }
    });

    $(document).on('submit', '.window form', function(event) {
        event.preventDefault();
        var $mensagens = $(this).parent().prev().children('textarea'); //Textarea de mensagens
        var $mensagem = $(this).children('input'); // Textarea input
        var mensagem = $mensagem.val();
        var destino = $(this).attr('id');
        $mensagens.append(' ' + usuarioAtivo + ': ' + mensagem + '\r\n');
        scrollDown($mensagens); // Efeito de rolagem
        socket.emit('enviarMensagem', {
            mensagem: mensagem,
            mensagemPara: destino
        });
        $mensagem.val('');
    });

    // Novas mensagens
    socket.on('novaMensagem', function(data) {
        var mensagemDe = data.mensagemDe;
        var mensagem = data.mensagem;
        var id = '#' + mensagemDe;
        var $id = $(id);

        if ($id.length === 0) {
            novaJanela(mensagemDe);
        }

        var $mensagens = $(id + ' > div.content > textarea');
        $mensagens.append(' ' + mensagemDe + ': ' + mensagem + '\r\n');
        scrollDown($mensagens); // Efeito de rolagem
    });

    // Atualização de usuários online e offline
    socket.on('atualizarListaUsuarios', function(data) {
        if (usuarioAtivo !== null) {
            var chat = $('#chat');
            if (chat.length) {
                listaUsuarios(data.usuarios);
            } else {
                $('#login').remove();
                $body.load('/public/chat.html', function() {
                    listaUsuarios(data.usuarios);
                });
            }
        }

    });

    function listaUsuarios(usuarios) {
        var html = '';
        for (var i = 0; i < usuarios.length; i++) {
            if (usuarios[i] != usuarioAtivo) {
                html += '<option>' + usuarios[i] + '</option>';
            }
        }
        $('#usuarios').html(html);
    }

    // Nova janela de conversação
    function novaJanela(idJanela) {
        var html = '<section id=' + idJanela + ' class="window">' +
            '<header>' +
            '<h1>' + idJanela + '</h1>' +
            '<button class="close">X</button>' +
            '</header>' +
            '<div class="content">' +
            '<textarea readonly></textarea>' +
            '</div>' +
            '<footer>' +
            '<form id=' + idJanela + ' class="window"> ' +
            '<input type="text" autofocus/>' +
            '<button type="submit">Enviar</button>' +
            '</form>' +
            '</footer>' +
            '</section>';
        var $containerWindows = $('#containerWindows');
        $containerWindows.html($containerWindows.html() + html);
    }

    function scrollDown($mensagens) {
        $mensagens.animate({
            scrollTop: $mensagens[0].scrollHeight - $mensagens.height()
        }, 0);
    }
});
